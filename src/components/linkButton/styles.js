import { StyleSheet } from "react-native";
import Colors from "../../assets/styles/colors";

export const BtnContainer = StyleSheet.create({
    common: {

    },
    primary: {
        marginHorizontal: 30,
        marginVertical: 26,
        width: 424,
        height: 78,
        paddingLeft: 35,
        justifyContent: "center",
        borderWidth: 2,
        backgroundColor: Colors.primary.green
    },
    default: {
        marginHorizontal: 30,
        marginVertical: 26,
        width: 424,
        height: 78,
        paddingLeft: 35,
        justifyContent: "center",
        borderWidth: 2,
        borderColor: Colors.secondary.white,
        backgroundColor: "transparent"
    },
    secondary: {
        marginHorizontal: 30,
        marginVertical: 26,
        width: 424,
        height: 78,
        paddingLeft: 35,
        justifyContent: "center",
        borderWidth: 2,
        backgroundColor: Colors.secondary.darkGray1
    },
    tertiaty: {
        marginHorizontal: 30,
        marginVertical: 26,
        width: 424,
        height: 78,
        paddingLeft: 35,
        justifyContent: "center",
        borderWidth: 2,
        borderColor: Colors.secondary.darkGray4,
        backgroundColor: Colors.secondary.white
    },
    navigationLink: {
        paddingHorizontal: 20,
        paddingTop: 12,
        marginHorizontal: 15,
    },
    navigationLinkActive: {
        borderTopWidth: 2,
        paddingTop: 10,
        borderColor: Colors.primary.green
    }
});

export const BtnImage = StyleSheet.create({
    common: {
        width: 20,
        height: 20,
        marginRight: 10,
        backgroundColor: "transparent",
        resizeMode: "contain"
    }
});

export const BtnText = StyleSheet.create({
    common: {
        fontFamily: "Arial",
        fontWeight: "bold",
    },
    primary: {
        fontSize: 16,
        color: Colors.secondary.white
    },
    secondary: {
        fontSize: 16,
        color: Colors.secondary.white
    },
    tertiaty: {
        fontSize: 16,
        color: Colors.secondary.darkGray4
    },
    default: {
        fontSize: 16,
        color: Colors.secondary.white
    },
    navigationLink: {
        fontSize: 12,
        color: Colors.secondary.white,
    }
});

